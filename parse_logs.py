import argparse
import os
import urllib

parser = argparse.ArgumentParser(description='Apache2 log parser.')
parser.add_argument('--path', help="Path to Apache2 log files", default="/var/log/apache2")
parser.add_argument('--top-urls', help="Find top URL-s", action='store_true')
parser.add_argument('--geoip', help="Resolve IP-s to country codes", action='store_true')
parser.add_argument('--verbose', help="Increase verbosity", action='store_true')

args = parser.parse_args()

keywords = "Windows", "Linux", "OS X", "Ubuntu", "Googlebot", "bingbot", "Android", "YandexBot", "facebookexternalhit"
d = {}
urls = {}
users = {}

total = 0
import gzip

for filename in os.listdir(args.path):
    if not filename.startswith("access.log"):
        continue
    if filename.endswith(".gz"):
        continue
        fh = gzip.open(os.path.join(args.path, filename))
    else:
        fh = open(os.path.join(args.path, filename))
    if args.verbose:
        print "Parsing:", filename
    for line in fh:
        total = total + 1
        try:
            source_timestamp, request, response, referrer, _, agent, _ = line.split("\"")
            method, path, protocol = request.split(" ")
            _, status_code, content_length, _ = response.split(" ")
            content_length = int(content_length)
            path=  urllib.unquote(path)
            if path.startswith("/~"):
                username, remainder = path[2:].split("/", 1)
                try:
                    users[username] = users[username] + content_length
                except:
                    users[username] = 1
                
            url = "http://enos.itcollege.ee" + path
            
            try:
                urls[url] = urls[url] + 1
            except:
                urls[url] = 1
                                            
            for keyword in keywords:
                if keyword in agent:
                    try:
                        d[keyword] = d[keyword] + 1
                    except KeyError:
                        d[keyword] = 1
                    break # Stop searching for other keywords
                                        
def humanize(bytes):
    if bytes < 1024:
        return "%d B" % bytes
    elif bytes < 1024 ** 2:
        return "%d kB" % (bytes / 1024.0)
    elif bytes < 1024 ** 3:
        return "%d MB" % (bytes / 1024.0 ** 2)
    else:
        return "%d GB" % (bytes / 1024.0 ** 3)

for filename in os.listdir("."):
    mode, inode, device, nlink, uid, gid, size, atime, mtime, ctime = os.stat(filename)
    print filename, humanize(size)

print("Top visited users") 
results = users.items()
results.sort(key = lambda item:item[1], reverse=True)
for user, transferred_bytes in results[:30]:
    print user, " ==>" , transferred_bytes / (1024 * 1024), "MB"
                     
print("Top visited URL-s") 
results = urls.items()
results.sort(key = lambda item:item[1], reverse=True)
for url, hits in results[0:5]:
    print url, " ==>" , hits, "(", hits * 100 / total, "%)"
    
    


